﻿using Mapbox.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCameraRaycst : MonoBehaviour
{
    [SerializeField] GameObject ApplyMenu;
    void Update()
    {

        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            Ray raycast = GetComponent<Camera>().ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {

                if (raycastHit.collider.CompareTag("GPSOBJECT"))
                {
                    ApplyMenu.SetActive(true);
                    gameEntryDB ge = raycastHit.collider.GetComponent<GPSQuest>().questData;
                    ApplyMenu.GetComponent<InitApplyQuest>().UpdateData(ge.gameName, ge.description, JsonConvert.DeserializeObject<List<string>>(ge.base64Image));
                }
            }
        }
        if ((Input.GetMouseButtonUp(0)))
        {
            Ray raycast = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {

                if (raycastHit.collider.CompareTag("GPSOBJECT"))
                {
                    ApplyMenu.SetActive(true);
                    gameEntryDB ge = raycastHit.collider.GetComponent<GPSQuest>().questData;
                    ApplyMenu.GetComponent<InitApplyQuest>().UpdateData(ge.gameName, ge.description, JsonConvert.DeserializeObject<List<string>>(ge.base64Image));
                }
            }
        }
    }
}
