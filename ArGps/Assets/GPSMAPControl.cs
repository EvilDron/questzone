﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Generic;
using UnityEngine;


public class GPSMAPControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    GameObject metka;
    Test3DObjScan objSpawn;
    List<gameEntryDB> entries = new List<gameEntryDB>();
    void Start()
    {
        objSpawn = GetComponent<Test3DObjScan>();
        applyData();

    }
    public void applyData()
    {
        DataBaseInterface.handleGameList(spawnList);
    }

    private void spawnList(List<string> objs)
    {

        foreach (var i in objs)
        {

            DataBaseInterface.HandleGameEntry(i, it =>
            {

                var met = objSpawn.SpawnObjByCoords(metka, it.getLongitude(), it.getLatitude());
                met.GetComponent<GPSQuest>().questData = it.toGameEntryDB();
                entries.Add(it.toGameEntryDB());
            });
        }
    }
}
