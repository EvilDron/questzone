﻿using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test3DObjScan : MonoBehaviour
{
    [SerializeField]
    private AbstractMap map_;

    public GameObject SpawnObjByCoords(GameObject obj, double lat, double lon)
    {
        Vector3 pos = Conversions.GeoToWorldPosition(lat, lon, map_.CenterMercator, map_.WorldRelativeScale).ToVector3xz();
        return Instantiate(obj, pos, Quaternion.Euler(new Vector3(0, 0, 0)));
        
    }

}
