﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowImage : MonoBehaviour
{

    [SerializeField] private GameObject canvas;
    [SerializeField] private GameObject Image;
    public static ShowImage instance;
    
	

    void Start()
    {
        if (instance == null)
        { 
            instance = this; 
        }
        else if (instance == this)
        {
            Destroy(gameObject); 
        }

    }
    public void PopupImage(Texture2D texture)
    {
        var image = Instantiate(Image, canvas.transform) ;
        var rect = image.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(Screen.width - 100, (Screen.width - 100) * texture.height / texture.width);
        image.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0,texture.width, texture.height), new Vector2(0.5f, 0.5f));

    }
    public void PopupImage(Texture2D texture, GameObject canvasObj)
    {
        var image = Instantiate(Image, canvasObj.transform);
        var rect = image.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(Screen.width - 100, (Screen.width - 100) * texture.height / texture.width);
        image.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

    }
}
