﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using Firebase.Auth;
using UnityEngine.UI;
using System;

public class Auth : MonoBehaviour
{

    public static event Action<string, string> OnAuthComplete;
    public static event Action<string> OnAuthFailed;
    [SerializeField]
    private static bool connectOnStart = true;

    void Start()
    {
      
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
         .RequestServerAuthCode(false)
         .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        Debug.LogFormat("SignInOnClick: Play Games Configuration initialized");
        if (connectOnStart)
        {
            SignInWithPlayGames();
        }
    }


    public static void SignInWithPlayGames()
    {

        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        UnityEngine.Social.localUser.Authenticate((bool success) => {
            if (!success)
            {
                Debug.LogError("Failed to Sign into Play Games Services.");
                OnAuthFailed?.Invoke("Failed to Sign into Play Games Services.");
                return;
            }

            string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            if (string.IsNullOrEmpty(authCode))
            {
                Debug.LogError("Signed into Play Games Services but failed to get the server auth code.");
                OnAuthFailed?.Invoke("Signed into Play Games Services but failed to get the server auth code.");
                return;
            }
            Debug.LogFormat("SignInOnClick: Auth code is: {0}", authCode);

            Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);

            auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("Sign in was canceled.");
                    OnAuthFailed?.Invoke("Sign in was canceled..");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("Encountered an error: " + task.Exception);
                    OnAuthFailed?.Invoke("Encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("SignInOnClick: User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

            });
            Firebase.Auth.FirebaseUser user = auth.CurrentUser;
            if (user != null)
            {
                string playerName = user.DisplayName;
                string uid = user.UserId;
                OnAuthComplete?.Invoke(playerName, uid);

            }
        });
    }
}