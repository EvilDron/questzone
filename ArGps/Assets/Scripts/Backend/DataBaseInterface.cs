﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Mapbox.Json;
using Proyecto26;
using UnityEngine;
using UnityEngine.UI;



public class DataBaseInterface
{
    public static void AddNewGame(string name, string description, List<string> ImageBASE64, AndroidJavaObject location)
    {

        gameEntry game = new gameEntry(name, PlayerInfo.Uid,description, ImageBASE64, location);
        RestClient.Put("https://questzone-73731684.firebaseio.com/" + game.getName() + ".json", game.toGameEntryDB());
    }
    public static void AddNewGame(gameEntry game)
    {
        RestClient.Put("https://questzone-73731684.firebaseio.com/" + game.getName() + ".json", game.toGameEntryDB());
        addGameToPpList(game.getName());
    }
    public static void AddNewUser(string userId)
    {
        UserEntry uEntry = new UserEntry(userId);
        
        RestClient.Put("https://questzone-73731684.firebaseio.com/" + userId + ".json", uEntry.touEntryDB);
      
       
    }

    public static void HandleUserEntry(string userID, Action<UserEntry> handler)
    {
       
        RestClient.Get<uEntryDB>("https://questzone-73731684.firebaseio.com/" + userID + ".json").Then(response =>
        {
            var responseObject = response.toUserEntry();
            Debug.Log("Getting to handler");
            handler(responseObject);
            Debug.Log("handled");
            RestClient.Delete("https://questzone-73731684.firebaseio.com/" + userID + ".json").Then(it =>
           RestClient.Put("https://questzone-73731684.firebaseio.com/" + userID + ".json", responseObject.touEntryDB));
        });
    }
    public static void HandleGameEntry(string name, Action<gameEntry> handler)
    {
        RestClient.Get<gameEntryDB>("https://questzone-73731684.firebaseio.com/" + name + ".json").Then(response =>
        {
            gameEntry tmp = response.toGameEntry();
            handler(tmp);

            RestClient.Delete("https://questzone-73731684.firebaseio.com/" + name + ".json").Then(it => Debug.Log("deleted"));

            RestClient.Put("https://questzone-73731684.firebaseio.com/" + name + ".json", tmp.toGameEntryDB());
        });
    }
    public static void DeleteUserEntry(string userID)
    {
        RestClient.Delete("https://questzone-73731684.firebaseio.com/" + userID + ".json");
    }
    public static void DeleteGameEntry(string name)
    {
        RestClient.Delete("https://questzone-73731684.firebaseio.com/" + name + ".json");
    }
    public static void ExecuteIfEntryExists(string entryID, Action<string> handler)
    {
    
        var tmp = RestClient.Get("https://questzone-73731684.firebaseio.com/" + entryID + ".json").Then(response =>
        {
            if (response.Text == "null")
            {
                handler(entryID);
            }
        });

    }
    public static void addGameToPpList(string gameID)
    {
        RestClient.Get<pp>("https://questzone-73731684.firebaseio.com/pp.json").Then(response =>
        {
            var tmp = JsonConvert.DeserializeObject<List<string>>(response.gameList);
            tmp.Add(gameID);
            response.gameList = JsonConvert.SerializeObject(tmp);
            RestClient.Put("https://questzone-73731684.firebaseio.com/pp.json", response);
        });
    }
    public static void handleGameList(Action<List<String>> handler)
    {
        RestClient.Get<pp>("https://questzone-73731684.firebaseio.com/pp.json").Then(response =>
        {
            var tmp = JsonConvert.DeserializeObject<List<string>>(response.gameList);
            handler(tmp);

        });

    }

}
public class pp
{
   public string gameList;
}