﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{

    public static event Action<int> OnExpChanged;

    private static string uid_;
    private static string nick_;
    private static int level_;
    private static int exp_;


    public static string Uid => uid_;
    public static int Exp { get => exp_; set { exp_ = value; OnExpChanged.Invoke(exp_); } }
    public static int Level { get => level_; set { level_ = value; OnExpChanged.Invoke(level_); } }

    public static string Nick { get => nick_; set => nick_ = value; }

    private void Start()
    {
        DontDestroyOnLoad(this);
        Auth.OnAuthComplete += Init;
    }
    private static void SetupUser(UserEntry userEntry)
    {
        Level = userEntry.Level;
        Exp = userEntry.Experience;
 
    }
    private static void Init(string username, string uid)
    {
        nick_ = username;
        uid_ = uid;
        DataBaseInterface.ExecuteIfEntryExists(uid, DataBaseInterface.AddNewUser);
        DataBaseInterface.HandleUserEntry(uid, SetupUser);
        
    }

}
