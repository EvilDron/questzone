﻿using UnityEngine;
using System.Collections;
using System.IO;
using static NativeCamera;
using UnityEngine.UI;

public class PhotoController : MonoBehaviour
{
    [SerializeField]
    GameObject photoContainer;
    [SerializeField]
    GameObject photosGrid;
    [SerializeField]
    GameObject addImageButton;
    [SerializeField]
    GameObject imageHolder;

    public void SetAddButton()
    {
        addImageButton.transform.parent = photosGrid.transform;
        transform.SetAsFirstSibling();
    }

    public static PhotoController instance;
    public void AddPhoto(Texture2D texture, string path, GameObject holder, GameObject container, GameObject photoGrid = null)
    {
        if (photoGrid != null)
        {
            var photo = Instantiate(container);
            photo.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, 184 * texture.height / 346, texture.height), new Vector2(0.5f, 0.5f));
            photo.transform.SetParent(photoGrid.transform);
            photo.GetComponent<ImageMemory>().image = texture;
            
        }
        else
        {
            var photo = Instantiate(container);
            photo.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, 184 * texture.height / 346, texture.height), new Vector2(0.5f, 0.5f));
            photo.transform.SetParent(photosGrid.transform);
            if (photosGrid.transform.childCount == 5)
            {
                addImageButton.transform.parent = null;
            }
            photo.GetComponent<ImageMemory>().image = texture;
            photo.GetComponent<ImageMemory>().path = path;
        }
    }

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

    }

    public void TakePicture(int maxSize)
    {
        NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create a Texture2D from the captured image
                Texture2D texture = NativeCamera.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                if (texture.width > texture.height)
                {
                    //Error
                }
                else
                {
                    AddPhoto(texture, path, imageHolder, photoContainer);

                }
            }
        }, maxSize);

        Debug.Log("Permission result: " + permission);
    }
}