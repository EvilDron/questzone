﻿using Mapbox.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserEntry
{
    private const float levelMultiplicator = 0.5f;
    private string userId;
    private string name;
    private int level;
    private int experience;
    private int maxexp = 100;
    private List<string> createdID ;
    private List<string> finishedID;

    public string UserID => userId;
    public int Level => level;
    public int Experience => experience;
    public int Maxexp => maxexp;
    public string Name => name;
    public uEntryDB touEntryDB => new uEntryDB(userId, name, level, experience, JsonConvert.SerializeObject(createdID), JsonConvert.SerializeObject(finishedID));


    public UserEntry(string id, string name, int lvl, int exp, List<string> cr, List<string> fin)
    {
        userId = id;
        this.name = name;
        level = lvl;
        experience = exp;
        createdID = cr;
        finishedID = fin;
    }
    public UserEntry(string user_id)
    {
        createdID = new List<string>();
        finishedID = new List<string>();
        userId = user_id;
        level = 1;
        name = PlayerInfo.Nick;
        experience = 0;
    }
    public void AddFinished(gameEntry game)
    {
        finishedID.Add(game.getName());
        AddExperience(game.getReward());
    }

    public void AddCreated(gameEntry game)
    {
        Debug.Log(createdID.Count.ToString());
        createdID.Add(game.getName());
        Debug.Log(createdID.Count.ToString());
    }

    private void AddExperience(int delta)
    {
        experience += delta;
        while (experience >= maxexp)
        {
            level++;
            maxexp += 100;
            experience -= maxexp;
        }
    }

}
[Serializable]
public class uEntryDB
{
    public int level;
    public int experience;
    public string name;
    public string userId;
    public string createdID;
    public string finishedID;

    public uEntryDB(string userId, string name, int level, int experience, string createdID, string finishedID)
    {
        this.name = name;
        this.userId = userId;
        this.level = level;
        this.experience = experience;
        this.createdID = createdID;
        this.finishedID = finishedID;
    }
    public UserEntry toUserEntry()
    {
        List<string> created = JsonConvert.DeserializeObject<List<string>>(createdID);
        List<string> finished = JsonConvert.DeserializeObject<List<string>>(finishedID);
        return new UserEntry(userId,name, level, experience, created, finished);
    }
}
