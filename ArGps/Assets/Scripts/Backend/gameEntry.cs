﻿
using Mapbox.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameEntry
{
    private const int experienceMultiplicator = 10;
    public struct pt
    {
        public pt(double p1, double p2)
        {
            latitude = p1;
            longitude = p2;
            filepath = "";
        }
        public double longitude;
        public double latitude;
        public string filepath;
    }
    private string creatorID;
    private string gameName;
    private string description;
    private List<string> base64Image = new List<string>();
    private float difficulty;
    private int overallDificulty;
    private int overallOriginality;
    private int numberOfRates;
    private pt destination;
    private List<pt> markers = new List<pt>();

    private float originality;

    public gameEntry(string name, string creatorID, string descr, List<string> b64im, AndroidJavaObject loc)
    {

        gameName = name;
        description = descr;
        this.creatorID = creatorID;
        base64Image = b64im;
        overallDificulty = 0;
        overallOriginality = 0;
        numberOfRates = 0;
        difficulty = 1;
        originality = 1;
        destination.longitude= loc.Call<double>("getLongitude");
        destination.latitude = loc.Call<double>("getLatitude");
    }
    public gameEntry(string name, string creatorID,string descr, List<string> b64im, float difficulty, float originality, int overallDifficulty, int overallOriginality,int numberOfRates, double longitude, double latitude, List<pt> markers)
    {
        Debug.Log("gameEntry Costructor");
        gameName = name;
        this.creatorID = creatorID;
        description = descr;
        base64Image = b64im;
        this.overallDificulty = overallDifficulty;
        this.overallOriginality = overallOriginality;
        this.numberOfRates = numberOfRates;
        this.difficulty = difficulty;
        this.originality = originality;
        destination.longitude = longitude;
        destination.latitude = latitude;
        this.markers = markers;
    }
    public List<string> getImages => base64Image;
    public void addImage(string filepath)
    {
        base64Image.Add(filepath);
    }
    public void rate(int diff, int orig)
    {
        numberOfRates++;
        overallDificulty += diff;
        overallOriginality += orig;
        difficulty = overallDificulty / numberOfRates;
        originality = overallOriginality / numberOfRates;
    }
    public void AddMarker(pt marker)
    {
        markers.Add(marker);
    }

    public string getName() => gameName;
    public float getDifficulty()
    {
        return difficulty;
    }
    public float getOriginality()
    {
        return originality;
    }
    public string getDescription()
    {
        return description;
    }
    public List<string> getB64im()
    {
        return base64Image;
    }
    public void HandleCreatorName(Action<string>handler)
    {
        DataBaseInterface.HandleUserEntry(creatorID, it => handler(it.Name));
    }
    public List<pt> getMarkers()
    {
        return markers;
    }
    public int getReward()
    {
        return Mathf.RoundToInt(difficulty * experienceMultiplicator + originality * experienceMultiplicator);
    }
    public double getLongitude()
    { 
        return destination.longitude;
    }
    public double getLatitude()
    {
        return destination.latitude;
    }
    public gameEntryDB toGameEntryDB()
    {
        return new gameEntryDB(gameName, creatorID,description,JsonConvert.SerializeObject( base64Image), difficulty,originality, overallDificulty, overallOriginality, numberOfRates, destination.longitude, destination.latitude,JsonConvert.SerializeObject(markers));
    }
}
[Serializable]
public class gameEntryDB
{
    public string gameName;
    public string creatorID;
    public string description;
    public string base64Image;
    public float difficulty;
    public float originality;
    public int overallDificulty;
    public int overallOriginality;
    public int numberOfRates;
    public double longitude;
    public double latitude;
    public string markers;


    public gameEntryDB(string gameName, string creatorID, string description, string base64Image, float difficulty, float originality, int overallDificulty, int overallOriginality, int numberOfRates, double longitude, double latitude, string markers)
    {
        this.gameName = gameName;
        this.creatorID = creatorID;
        this.description = description;
        this.base64Image = base64Image;
        this.difficulty = difficulty;
        this.originality = originality;
        this.overallDificulty = overallDificulty;
        this.overallOriginality = overallOriginality;
        this.numberOfRates = numberOfRates;
        this.longitude = longitude;
        this.latitude = latitude;
        this.markers = markers;
    }
    public gameEntry toGameEntry()
    {
        return new gameEntry(gameName, creatorID, description, JsonConvert.DeserializeObject<List<string>>(base64Image), difficulty, originality, overallDificulty, overallOriginality, numberOfRates, longitude, latitude, JsonConvert.DeserializeObject<List<gameEntry.pt>>(markers));
    }
}
