﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class AddQuest : MonoBehaviour
{
    [SerializeField] Text LabelText;
    [SerializeField] Text DescriptionText;
    [SerializeField] Text ExpText;
    private string Label;
    private string Description;
    private int ExperienceReward;
    private List<gameEntry.pt> coords = new List<gameEntry.pt>();
    private List<string> imagePaths = new List<string>();
    void CollectInfo()
    {
        Label = LabelText.text;
        Description = DescriptionText.text;
      //  ExperienceReward = int.Parse(ExpText.text);
        var images = GameObject.FindObjectsOfType<ImageMemory>();
        foreach (var obj in images)
        {
            imagePaths.Add(Convert.ToBase64String(File.ReadAllBytes(@obj.path)));
        }
        var markers = GameObject.FindObjectsOfType<Marker>();
        foreach (var obj in markers)
        {
            gameEntry.pt temp = new gameEntry.pt(obj.Lat,obj.Lon);
            coords.Add(temp);
        }
    }
    public void Push()
    {
        CollectInfo();
        Apply();
    }
    void Apply()
    {
        var newGame = new gameEntry(Label, PlayerInfo.Uid, Description, imagePaths, 1, 1, 0, 0, 0, coords[coords.Count - 1].latitude, coords[coords.Count - 1].longitude, coords);
        
        DataBaseInterface.AddNewGame(newGame);
    }
}
