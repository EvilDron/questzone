﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpirienceBar : MonoBehaviour
{
    [SerializeField] Text expText;
    [SerializeField] Text lvlText;
    [SerializeField] private int maxExp;
    [SerializeField] private float maxSize;

    private RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        PlayerInfo.OnExpChanged += ExpUpdated;
    }
    private void ExpUpdated(int exp)
    {
        rect.sizeDelta = new Vector2(exp * maxSize / maxExp, rect.sizeDelta.y);
        expText.text = $"{exp}/{maxExp}";

    }
    private void Lvl(int level)
    {
        lvlText.text = $"Level  {level}";
    }
}
