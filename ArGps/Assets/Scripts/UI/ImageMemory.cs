﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageMemory : MonoBehaviour
{

    public void OpenImage()
    {
        ShowImage.instance.PopupImage(image);
    }
    public void OpenCanvasImage()
    {
        ShowImage.instance.PopupImage(image, GameObject.FindWithTag("distanceText"));
        
    }
    public void SendDestroy()
    {
        PhotoController.instance.SetAddButton();
    }
    public Texture2D image;
    public string path;
}
