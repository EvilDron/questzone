﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BarLabel : MonoBehaviour
{

    [SerializeField]
    private int min;
    [SerializeField]
    private int max;

    private Text text;


    private void Start()
    {
      
        Debug.Log("pp");

      
        text = GetComponent<Text>();
    }
    public void UpdateValue(float scrollvalue)
    {
        Debug.Log(scrollvalue);
        text.text = Mathf.RoundToInt(min + (max - min) * scrollvalue).ToString();
    }
}
