﻿using Mapbox.Json;
using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    [SerializeField] GameObject MainMenu;
    [SerializeField] GameObject GPSMAP;
    [SerializeField] GameObject ARMarker;
    [SerializeField] GameObject MarkersGrid;
    public static WindowManager instance;


    public void SuccecFullObjectAdded(double lat, double lon)
    {
        DisableAR(false);
        GameObject.Find("MyQuestText").GetComponent<Text>().text = $"{lat}  {lon}";
        Marker marker = Instantiate(ARMarker).GetComponent<Marker>();
        marker.Lat = lat;
        marker.Lon = lon;
        marker.transform.SetParent(MarkersGrid.transform);
        marker.transform.SetAsFirstSibling();
        

    }

    void Start()
    {
        
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
        
       MainMenu = GameObject.Find("MainMenu");
        MarkersGrid = GameObject.Find("MarkerGrid");
    }
    public void EnableAR()
    {
        MainMenu.SetActive(false);
        GPSMAP.SetActive(false);
        SceneManager.LoadSceneAsync("AddMarker", LoadSceneMode.Additive);

    }
    public void DisableAR(bool MAP)
    {

        SceneManager.UnloadSceneAsync("AddMarker");
        if (MAP)
        {
            GPSMAP.SetActive(true);
        }
        else
        {
            MainMenu.SetActive(true);
        }
    }
    public void EnableGPS()
    {
        MainMenu.SetActive(false);
        GPSMAP.SetActive(true);
    }
}
