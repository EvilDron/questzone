﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTap : MonoBehaviour
{
    
    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
