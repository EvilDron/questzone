﻿using Mapbox.Unity.Location;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceptARObject : MonoBehaviour
{
    public double Lat = 0;
    public double Lng = 0;

    public void Accept()
    {
        WindowManager.instance.SuccecFullObjectAdded(Input.location.lastData.latitude, Input.location.lastData.longitude);
    }
}
