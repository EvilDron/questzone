﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsernameText : MonoBehaviour
{
    private Text text;
    void Start()
    {
        text = GetComponent<Text>();
        Auth.OnAuthComplete += SetNameText;
    }

    private void SetNameText(string username, string uid)
    {
        text.text = username;
    }
}
