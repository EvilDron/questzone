﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject PlayerInfoPanel;
    [SerializeField] GameObject AddPanel;
    [SerializeField] GameObject BottomPanel;
    // Start is called before the first frame update
   public void GoToAdd()
    {
        AddPanel.SetActive(true);
        PlayerInfoPanel.SetActive(false);
        BottomPanel.SetActive(false); ;
    }
    public void GoToMenu()
    {
        AddPanel.SetActive(false);
        PlayerInfoPanel.SetActive(true);
        BottomPanel.SetActive(true); ;
    }
}
