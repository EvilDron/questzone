﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField]
    private Transform ARCam;
    // Update is called once per frame
    public void UpdateRot()
    {
        transform.rotation = Quaternion.Euler(90, ARCam.rotation.y, 0);
    }
}
