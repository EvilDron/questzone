﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public float speed = 0.1F;
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(-touchDeltaPosition.x * speed, 0f,-touchDeltaPosition.y * speed, Space.World);
        }
    }
}