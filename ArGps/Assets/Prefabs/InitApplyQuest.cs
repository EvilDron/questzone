﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitApplyQuest : MonoBehaviour
{
    [SerializeField] Text LabelText;
    [SerializeField] Text DescriptionText;
    [SerializeField] GameObject holder;
    [SerializeField] GameObject photoContainer;
    private List<string> imgs = new List<string>();

    public void UpdateData(string label, string description, List<string> imgs)
    {
        Debug.Log("SANYA  "+imgs.Count.ToString());
        LabelText.text = label;
        DescriptionText.text = description;
        foreach (var obj in imgs)
        {
            byte[] imageBytes = Convert.FromBase64String(obj);
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(imageBytes);
            PhotoController.instance.AddPhoto(tex,"",holder, photoContainer, holder);
        }
    }
}
